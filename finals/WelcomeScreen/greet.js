import React, { useRef, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Animated} from 'react-native';


//Animated fading in view
const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }
    ).start();
  }, [fadeAnim])

  return (
    <Animated.View                 
      style={{
        ...props.style,
        opacity: fadeAnim,         // Bind opacity to animated value
      }}
    >
      {props.children}
    </Animated.View>
  );
}

//Animated ease in for Toe
 const Tic = (props) => {
      const translation = useRef(new Animated.Value(0)).current
   
    Animated.timing(translation, {
      useNativeDriver: true,
      toValue: 1,
      duration: 1000
    }).start();
     

    return(
       <Animated.View
      style={{
        transform: [
          { translateX: translation },
        ],
       
      }}
    >
      <Text style={styles.welcome1}>Tic</Text>
      </Animated.View>
    )
    
  };

//Animated ease in for Tac
   const Tac = (props) => {
      const translation = useRef(new Animated.Value(0)).current
   
    Animated.timing(translation, {
      useNativeDriver: true,
      toValue: 120,
      duration: 1000
    }).start();
     

    return(
       <Animated.View
      style={{
        transform: [
          { translateX: translation },
        ],
       opacity: translation.interpolate({
  inputRange: [0, 120],
  outputRange: [0, 1],
}),
      }}
    >
      <Text style={styles.welcome2}>Tac</Text>
      </Animated.View>
    )
    
  };
  
  //Animated ease in for Toe
  const Toe = (props) => {
      const translation = useRef(new Animated.Value(0)).current
   
    Animated.timing(translation, {
      useNativeDriver: true,
      toValue: 245,
      duration: 1000
    }).start();
     
    return(
       <Animated.View
      style={{
        transform: [
          { translateX: translation },
        ],
       opacity: translation.interpolate({
  inputRange: [0, 245],
  outputRange: [0, 1],
}),
      }}
    >
      <Text style={styles.welcome1}>Toe</Text>
      </Animated.View>
    )  
  };

//Animated Lines
  const Line = (props) => {
      const translation = useRef(new Animated.Value(0)).current
  
   useEffect(() => {
    Animated.timing(translation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }, [translation]);
     
    return(
       <Animated.View
      style={{
        height: 2,
        width: 300,
        backgroundColor: 'black',
        transform: [
          { translateX: translation },
        ],
       opacity: translation.interpolate({
  inputRange: [0, 1],
  outputRange: [0, 1],
}),
      }}
    />

    )  
  };

export default function WelcomeScreen({ navigation }) {

  return (
    <View style={styles.container}>
    <FadeInView>
    <Line/>
     <Tic>
     </Tic>
     <Line/>
      <Tac/>
      <Line/>
      <Toe/>
      <Line/>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PVPStage')}>
        <Text style={styles.text}>Play Against A Player</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('EasyStage')}>
        <Text style={styles.text}>Play Against EasyBot</Text>
      </TouchableOpacity>
      </FadeInView>
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'orange'
  },
  welcome1: {
    letterSpacing: 1,
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'left',
    textShadowColor: 'black',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    
  },
   welcome2: {
    color: 'white',
    letterSpacing: 1,
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'left',
    textShadowColor: 'black',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    
  },
  button: {
   alignItems:'center',
    height: 30,
    width: 270,
    borderRadius: 10,
    backgroundColor: 'white',
    margin: 10,
    shadowOpacity: 1.5,
    elevation: 10,
    shadowRadius: 10 ,
    shadowOffset : { width: 1, height: 13},
  },
  text: {
  fontWeight: 'bold',
   textAlign: 'center',
    color: 'grey',
    fontSize: 15,
    marginTop: 3,
    
  }
});