import React, { useState, useEffect, useRef } from 'react';
import {StyleSheet, View,Text,TouchableNativeFeedback, Animated } from 'react-native';


//Animated fading in view
const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 0

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }
    ).start();
  }, [fadeAnim])

  return (
    <Animated.View                 
      style={{
        ...props.style,
        opacity: fadeAnim,         // Bind opacity to animated value
      }}
    >
      {props.children}
    </Animated.View>
  );
}

export default function PVP ({navigation}) { 
    
  const [gameState, setGameState] = useState([[], [], []]); // Game Board
  const [currentPlayer, setCurrentPlayer] = useState(1);    
  const [gameIsOver, setGameIsOver] = useState(false);
  const [winner, setWinner] = useState(0);
  const [humanVsAi, setHumanVsAi] = useState(false);

  useEffect(() => {
    startGame(true);
  }, []);

  const startGame = (humanVsAi) => {
    setGameState(
        [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ]
    );
    setCurrentPlayer(1);
    setGameIsOver(false);
    setWinner(0);
    setHumanVsAi(humanVsAi)
  }

  const getWinner = () => {
      const tilesRequiredToWin = 3;
      let sum;
      let currentGameState = gameState.slice();
    
      //Victory Conditions (Horizontal, Vertical)
      for (let i = 0; i < tilesRequiredToWin; i++) {
          sum = currentGameState[i][0] + currentGameState[i][1] + currentGameState[i][2];
          if (sum === 3) {
              return 1;
          } else if (sum === -3) {
              return -1;
          }
      }

      for (let i = 0; i < tilesRequiredToWin; i++) {
          sum = currentGameState[0][i] + currentGameState[1][i] + currentGameState[2][i];
          if (sum === 3) {
              return 1;
          } else if (sum === -3) {
              return -1;
          }
      }
      //Victory Conditions (Diagonal)
      sum = currentGameState[0][0] + currentGameState[1][1] + currentGameState[2][2];
      if (sum === 3) {
          return 1;
      } else if (sum === -3) {
          return -1;
      }

      sum = currentGameState[2][0] + currentGameState[1][1] + currentGameState[0][2];
      if (sum === 3) {
          return 1;
      } else if (sum === -3) {
          return -1;
      }

      return 0;
  }

  //Condition if all tiles are filled and no winner
  const checkTie = () => {
      let gameIsTie = false;
      let currentGameState = gameState.slice();
      let tilesPressed = 0;

      for (let i = 0; i < currentGameState.length; i++) {
          for (let j = 0; j < currentGameState.length; j++) {
              if (currentGameState[i][j] !== 0) {
                  tilesPressed += 1;
              }
          }
      }

      if (tilesPressed === 9) {
          gameIsTie = true;
      }

      return gameIsTie;
  }


  const pressTile = (row, col, humanVsAi) => {
      const pressedTile = gameState[row][col];

      //Switching between players
      if (humanVsAi) {
          if (pressedTile === 0 && !gameIsOver) {
              const updatedGameState = gameState.slice();

              updatedGameState[row][col] = currentPlayer;
              setGameState(updatedGameState);

              const nextPlayer = (currentPlayer == 1) ? -1 : 1;
              setCurrentPlayer(nextPlayer);
          }
      } 

          const winner = getWinner();

          if (winner === 1) {
              setGameIsOver(true);
              setWinner(1);
          } else if (winner === -1) {
              setGameIsOver(true);
              setWinner(-1);
          } else {
              const gameIsOver = checkTie();
              if (gameIsOver) {
                  setGameIsOver(true);
              }
          }
      }
  
    //Player Tics and Tacs
  const renderIcon = (row, col) => {
      const value = gameState[row][col];
      switch (value) {
          case 1: 
              return (
                <Text style={styles.circle}> O </Text>
              );
          case -1:
              return (
                <Text style={styles.cross}> X </Text>
              );
          default:
              return null;
      }
  }

  //Game over message
  const renderGameOverMessage = (humanVsAi) => {
      switch (winner) {
          case 1:
              return (
                  <View style={styles.gameOverMessageContainer}>
                      <Text>Game over</Text>
                      <Text>Player 1 won!</Text>
                  </View>
              );
          case -1:
              return (
                  <View style={styles.gameOverMessageContainer}>
                      <Text>Game over</Text>
                          <Text>Player 2 won!</Text>
                  </View>
              );
          default:
              return (
                  <View style={styles.gameOverMessageContainer}>
                      <Text>Game over</Text>
                      <Text>The game ended in a draw!</Text>
                  </View>
              );
      }
  }
  
  //Player Scores and Scoreboard
   const gameScore = (humanVsAi) => {

     var pieces = {
       'X': 0, 'O': 0
     }
      switch (winner) {
          case 1:
                pieces['O'] += 1;
                break;
              
          case -1:
              pieces['X'] += 1;
                break;
          default:
              null;
              break;
      }

return(
     <View style={styles.scores_container}>
   <View style={styles.score}>
						<Text style={styles.user_score}>Player 1: {pieces['O']}</Text>
					</View>
    	<View style={styles.score}>
						<Text style={styles.ai_score}>Player 2: {pieces['X']}</Text>
					</View>
    </View>
  )

  }

  return (
    
    <View style={styles.container}>
    <FadeInView>
      {
          (gameIsOver) &&
              <View>
                  {renderGameOverMessage(humanVsAi)}
              </View>
      }
      <View style={{ flexDirection: 'row' }}>
          <TouchableNativeFeedback 
              onPress={() => pressTile(0, 0, humanVsAi)} 
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderLeftWidth: 0, borderTopWidth: 0 }]}>
                  {renderIcon(0, 0)}
              </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback 
              onPress={() => pressTile(0, 1, humanVsAi)}
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderTopWidth: 0 }]}>
                  {renderIcon(0, 1)}
              </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback 
              onPress={() => pressTile(0, 2,humanVsAi)} 
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderRightWidth: 0, borderTopWidth: 0 }]}>
                  {renderIcon(0, 2)}
              </View>
          </TouchableNativeFeedback>
      </View>
      <View style={{ flexDirection: 'row' }}>
          <TouchableNativeFeedback 
              onPress={() => pressTile(1, 0,humanVsAi)} 
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderLeftWidth: 0 }]}>
                  {renderIcon(1, 0)}
              </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback 
              onPress={() => pressTile(1, 1,humanVsAi)} 
              disabled={gameIsOver}>
              <View style={styles.tile}>
                  {renderIcon(1, 1)}
              </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback 
              onPress={() => pressTile(1, 2,humanVsAi)} 
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderRightWidth: 0 }]}>
                  {renderIcon(1, 2)}
              </View>
          </TouchableNativeFeedback>
      </View>
      <View style={{ flexDirection: 'row' }}>
          <TouchableNativeFeedback 
              onPress={() => pressTile(2, 0,humanVsAi)} 
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderLeftWidth: 0, borderBottomWidth: 0 }]}>
                  {renderIcon(2, 0)}
              </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback 
              onPress={() => pressTile(2, 1,humanVsAi)} 
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderBottomWidth: 0 }]}>
                  {renderIcon(2, 1)}
              </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback 
              onPress={() => pressTile(2, 2,humanVsAi)} 
              disabled={gameIsOver}>
              <View style={[styles.tile, { borderRightWidth: 0, borderBottomWidth: 0 }]}>
                  {renderIcon(2, 2)}
              </View>
          </TouchableNativeFeedback>
      </View>
      <View>
      {gameScore(humanVsAi)}
      </View>
      <TouchableNativeFeedback onPress={() => startGame(humanVsAi)}>
          <View style={styles.again}>
              <Text>New Game</Text>
          </View>
      </TouchableNativeFeedback>
    <TouchableNativeFeedback onPress={() => navigation.navigate('Welcome')}>
          <View style={styles.home}>
              <Text>Return Home</Text>
          </View>
      </TouchableNativeFeedback>
     </FadeInView>
    </View>
  );
};


const styles = StyleSheet.create({

  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
  },

  tile: {
      borderWidth: 1,
      width: 100,
      height: 100,
      backgroundColor: 'darkgrey',
      borderColor: '#ffff'

  },
  home:{
    padding: '5%',
    margin: '1%',
    backgroundColor: 'powderblue', 
    borderRadius: 10, 
    width: '100%', 
    left: 85
  },
  again:{
    padding: '5%',
    margin: '1%',
    backgroundColor: 'powderblue', 
    borderRadius: 10, 
    width: '100%', 
    left: 93
  },
  gameOverMessageContainer: {
      paddingBottom: '10%',
      justifyContent: 'center',
      alignItems: 'center' 
  },
   scores_container: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	score: {
		alignItems: 'center'
	},
	user_score: {
		fontSize: 25,
		fontWeight: 'bold'
	},
  ai_score: {
    marginLeft: 40,
		fontSize: 25,
		fontWeight: 'bold'
	},
    circle: {
       marginLeft: 5,
		fontSize: 70,
		fontWeight: 'bold',
        color: 'white'
	},
    cross: {
        marginLeft: 5,
         fontSize: 70,
         fontWeight: 'bold',
         color: 'black'
     },
});

