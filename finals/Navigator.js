import { createStackNavigator } from 'react-navigation-stack';
import WelcomeScreen from './WelcomeScreen/greet';
import EasyStage from './GameScreen/easystage';
import PVP from './GameScreen/pvp';

export default createStackNavigator(
  {
    Welcome: {
      screen: WelcomeScreen,

    },
    EasyStage: {
      screen: EasyStage,
    },
    PVPStage: {
      screen: PVP,
    },
  },
  {
    navigationOptions: {
      title: 'Tic Tac Toe',
    },
    headerMode: {
      headerShown: false
    }
  },
);