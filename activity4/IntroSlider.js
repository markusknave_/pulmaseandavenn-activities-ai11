import React from "react";
import { Image, Text, View } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

//contents of the slide
const slides = [
  {
    key: "one",
    title: "Why IT?", 
    text:
      "Let's find out!",
    image: require("./images/uChoice.png"),
  },
  {
    key: "two",
    title: "IT IS GLOBALIZED",
    text:
      "Technology is everywhere and there are still more to develop for the digital world!",
    image: require("./images/uAdventure.png"), 
  },
  {
    key: "three",
    title: "DIGITAL CAREER",
    text:
      "As we move more towards an era where software is as important as hardware, career opportunities are sure to arise!",
    image: require("./images/uCareerProg.png"),
  },
  {
    key: "four",
    title: "REALIZATION & INTEREST",
    text:
      "I realized that developing games and applications are not that easy, so I am eager to learn more about it",
    image: require("./images/uThinking.png"),
  },
    {
    key: "five",
    title: "DIGITAL CREATIVITY",
    text:
      "As an individual whom is fond of creative writing, IT was introduced to me as another world of writing that comes to life",
    image: require("./images/uVision.png"),
  },
    {
    key: "six",
    title: "CHALLENGED YET ENJOYING",
    text:
      "Ever since I learned HTML, I struggled to understand the language yet I am willing to learn and develop from differed softwares!",
    image: require("./images/uCoder.png"),
  },
]

const Slide = ({ item }) => {
    return (
      <View style={{ flex: 1, backgroundColor: 'white'}}>
        <Image source={item.image}
          style={{
            resizeMode: 'contain',
            height: "70%",
            width: "100%",
          }}
        />
        <Text style={{
            paddingTop: 5,
            paddingBottom: 5,
            fontSize: 20,
            fontWeight: "bold",
            color: "#21465b",
            alignSelf: "center",
          }}
        >
          {item.title}
        </Text>

        <Text style={{
          textAlign:"center",
          color:"#b5b5b5",
          fontSize:15,
          paddingHorizontal:30,
        }}>
          {item.text}
        </Text>
      </View>
    )
  };

 export default function IntroSliders({navigation}) {
   return(
       <AppIntroSlider
          renderItem={({item}) => <Slide item={item}/> } 
          data={slides} 
          activeDotStyle={{
            backgroundColor:"#21465b",
            width:10
          }}
          onDone={() => navigation.push('Intro')}
          showDoneButton={true}
          renderDoneButton={()=> <Text style={{color: '#21465b', fontWeight: 'bold', margin: 1, fontSize: 18}}>
              Home</Text>}
          showNextButton={true}
          renderNextButton={()=> <Text style={{color: '#21465b', fontWeight: 'bold', margin: 1, fontSize: 18}}>
              Continue</Text>}
         /> 
    ) 
};