import { SafeAreaView,StyleSheet, TextInput, Button, Alert} from 'react-native';
import React from "react";

const UserTextInput = () => {
  const [text, onChangeText] = React.useState('Enter a Text');

  return (
    <SafeAreaView style = {styles.container}>
      <TextInput
        style = {styles.input}
        onChangeText={onChangeText}
        value={text}
      />
      <Button color = 'darkorange' 
      title = "Submit" 
      onPress={() => Alert.alert('Alert', 'You just typed: "' + text + '"')} />
    </SafeAreaView>
  );
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'orange',
    justifyContent: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    textAlign: 'center',
    backgroundColor: 'white',
  },
});

export default UserTextInput;