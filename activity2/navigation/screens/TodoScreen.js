import { StyleSheet, Text, TextInput, View, ScrollView, Keyboard, KeyboardAvoidingView, Platform, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import Task from '../../components/Task';


export default function TodoScreen ({navigation}){
  const [task, setTask] = useState();
  const [taskItems, setTaskItems] = useState([]);

  const handleAddTask = () => {
    Keyboard.dismiss();
    setTaskItems([...taskItems, task])
    setTask(null);
  }

  const completeTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy)
  }

  return (
    <View style = {styles.container}>
            <ScrollView contentContainerStyle={{ flexGrow: 1}} keyboardShouldPersistTaps='handled'>

      <View style = {styles.tasksWrapper}>
          <Text style = {styles.sectionTitle}> Tasks for Today </Text>
          <View style = {styles.items}>
          {taskItems.map((item, index) => {
              return (
                <TouchableOpacity key={index}  onPress={() => completeTask(index)}>
                  <Task text={item} />
                </TouchableOpacity>
              )
            }).reverse()
          }
        </View>
      </View>
        
      </ScrollView>

        <KeyboardAvoidingView
          behavior = {Platform.OS === 'android' ? 'padding' : 'height'}
          style = {styles.writeTaskWrapper}>

            <TextInput style = {styles.input} placeholder={'Add a task'} 
            value={task} onChangeText={text => setTask(text)}/>

            <TouchableOpacity onPress={() => handleAddTask()}>
              <View style = {styles.addWrapper}>
                <Text>+</Text>
              </View>
            </TouchableOpacity>
          </KeyboardAvoidingView>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
  },
  tasksWrapper: {
      paddingTop: 40,
      paddingHorizontal: 60,
  },
  sectionTitle: {
      top: 60,
      fontSize: 20,
      fontFamily: "serif",
      paddingBottom: 10,
      fontWeight: 'bold',
      borderBottomWidth: 3,
      alignItems: 'center',
  },
  items: {
      marginTop: 85,
  },
  writeTaskWrapper: {
      position: 'absolute',
      top: 30,
      width: '100%',
      flexDirection: 'row-reverse',
      justifyContent: 'space-around',
      alignItems: 'center',
      marginBottom: 10,
  },
  input: {
      paddingVertical: 10,
      paddingHorizontal: 15,
      backgroundColor: 'gray',
      borderRadius: 60,
      borderColor: 'black',
      borderWidth: 1,
      width: 250,

},
  addWrapper: { 
      width: 60,
      height: 60,
      backgroundColor: 'gray',
      borderRadius: 60,
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: 'black',
      borderWidth: 1,
},
});