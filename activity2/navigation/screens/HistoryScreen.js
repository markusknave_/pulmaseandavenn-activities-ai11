import * as React from 'react';
import {View, Text} from 'react-native';

export default function HistoryScreen() {
    return(
        <View style = {{backgroundColor: '#E8EAED',flex:1, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 24, fontWeight: 'bold'}}>Completed Tasks go here</Text>
        </View>
    )
}