import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HistoryScreen from './navigation/screens/HistoryScreen.js';
import TodoScreen from './navigation/screens/TodoScreen.js';



const Tab = createBottomTabNavigator();

export default function App() {
  return(
    <NavigationContainer>
    <Tab.Navigator
    initialRouteName= 'ToDo'
    screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
        let thisIcon;
        let destination = route.name;

        if (destination === 'ToDo') {
            thisIcon = focused ? 'list' : 'list-outline';
        } else if (destination === 'History') {
            thisIcon = focused ? 'library' : 'library-outline';
        }
    
        return <Ionicons name={thisIcon} size={size} color={color} />;
    },
    tabBarActiveTintColor: '#e88413',
    tabBarInactiveTintColor: 'gray',
    tabBarLabelStyle: { paddingBottom: 10, fontSize: 10},
    tabBarStyle: {padding: 10, height: 60}
    })}>

<Tab.Screen name= 'ToDo' component={TodoScreen} options={{headerShown: false}} />
<Tab.Screen name= 'History' component={HistoryScreen} 
options={{title: 'History', headerTitleStyle: {marginLeft:105}}} />
</Tab.Navigator>
</NavigationContainer>
  );
}