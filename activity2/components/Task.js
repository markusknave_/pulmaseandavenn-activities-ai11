import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default function Task (props) {

    return (
        <View style = {styles.item}>
            <View style = {styles.itemLeft}>
                <View style = {styles.square}></View>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'black',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
},
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
},
    square: {
        width: 24,
        height: 24,
        backgroundColor: 'black',
        opacity: 0.3,
        borderRadius: 5,
        marginRight: 15,
},
    itemText: {
        maxWidth: '75%',
},
});